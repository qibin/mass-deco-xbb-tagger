import pandas as pd
import glob
import h5py as h5
import numpy as np
import argparse,math,os
import matplotlib.pyplot as plt
plt.switch_backend('agg')
import psutil
import gc
import cPickle as pk
from matplotlib.ticker import FormatStrFormatter
import infoJZXW

GeV=1000

parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("--in", dest='inp',  default="./test/test_data/", help="path to in dir")
parser.add_argument("--out", dest='out',  default="./test/test_output/", help="pathout")
args = parser.parse_args()

files = glob.glob(args.inp + "/*_D.h5")
key="fat_jet"
variables=["pt","mcEventWeight"]


def getJZX(filename):
	#see https://gitlab.cern.ch/atlas-boosted-hbb/xbb-datasets/blob/master/p3652/mc16a-input-datasets.txt
	#file name is like "user.dguest.16843503._000001.output_361020_D.h5"
	idString=filename.split("_")[-2]
	if len(idString)!=6 : 
		print "Error file : {}".format(filename)
		return None
	id=int(idString)
	X=id-361020 #361020=JZ0W, so on
	if X < 0 :
		print "Not JZXW Dijet : {}".format(filename)
		return None
	return X

def getEvtNum(filename):
	metadata=h5.File(filename,"r")["metadata"]
	evtNum=metadata["nEventsProcessed"]
	del metadata
	gc.collect()
	return evtNum

def calcuRwt(JZX,EvtNum):
	#see https://twiki.cern.ch/twiki/bin/view/Atlas/NtuplesForTLA
	#see https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/JetEtMissMCSamples
	crossSec=float(infoJZXW.info[JZX]['xsect_nb'])
	filterEff=float(infoJZXW.info[JZX]['filt_eff'])
	Rt=crossSec*filterEff/EvtNum
	print "Rt JZ{}W : {} * {} / {} = {}".format(JZX,crossSec,filterEff,EvtNum,Rt)
	return Rt

Data={}
Wt={} 
rWt={}

EvtNum={}
Rt={} #Note rWt=Wt*Rt ,Rt is same for same sample e.g. JZ0W
Size=0
Events=0
for file in files:
	if getJZX(file) is None : continue
	JZX=getJZX(file)
	catlog="JZ{:0>2d}W".format(JZX) #catlog is JZXXW
	data=pd.read_hdf(file,"fat_jet")[variables].dropna() #[[]] means return dataFrame
	h5file=h5.File(file,"r")
	size=data["pt"].size
	evtNum=getEvtNum(file)
	if catlog not in Data.keys() :
		Data[catlog]=np.divide(data["pt"], GeV) # [] return np.arrary
		Wt[catlog]=data["mcEventWeight"]
		EvtNum[catlog]=evtNum
		print "processed {} : new jets,evts {},{}".format(catlog,Data[catlog].size, EvtNum[catlog])
	else :
		Data[catlog]=np.concatenate((Data[catlog],np.divide(data["pt"], GeV)))
		Wt[catlog]=np.concatenate((Wt[catlog],data["mcEventWeight"]))
		EvtNum[catlog] += evtNum
		print "processed {} : append jets,evts {},{}, total {},{}".format(catlog,size,evtNum,Data[catlog].size,EvtNum[catlog])
	mems1=psutil.Process(os.getpid()).memory_info().rss
	Size+=size
	Events+=evtNum
	del data
	gc.collect()
	mems2=psutil.Process(os.getpid()).memory_info().rss
	print "mem:{}M -> {}M".format(mems1/1000000,mems2/1000000)

mems1=psutil.Process(os.getpid()).memory_info().rss
for catlog in Wt.keys():
        Rt[catlog]=calcuRwt(int(catlog[2:4]),EvtNum[catlog])
        rWt[catlog]=Wt[catlog]*Rt[catlog]
	#print "Rt {} : {}".format(catlog,Rt[catlog])
mems2=psutil.Process(os.getpid()).memory_info().rss
print "reWt calcu. MEM:{}M -> {}M".format(mems1/1000000,mems2/1000000)

mems1=psutil.Process(os.getpid()).memory_info().rss
#DATA=np.zeros(Size)
#WT=np.zeros(Size)
Data["ToT"]=np.concatenate(Data.values())
Wt["ToT"]=np.concatenate(Wt.values())
rWt["ToT"]=np.concatenate(rWt.values())
mems2=psutil.Process(os.getpid()).memory_info().rss
print "conc. MEM:{}M -> {}M".format(mems1/1000000,mems2/1000000)

outData=args.out+"Data.pickle"
outWt=args.out+"Wt.pickle"
outrWt=args.out+"rWt.pickle"
fData= open(outData, 'wb')
fWt= open(outWt, 'wb')
frWt= open(outrWt, 'wb')
pk.dump(Data,fData,pk.HIGHEST_PROTOCOL)  #using highest protocol: binary e.g.
pk.dump(Wt,fWt,pk.HIGHEST_PROTOCOL)
pk.dump(rWt,frWt,pk.HIGHEST_PROTOCOL)
print "pickle finished: {}:{}M, {}:{}M".format(outData,os.path.getsize(outData)/1024/1024,outWt,os.path.getsize(outWt)/1024/1024)
fData.close()
fWt.close()
frWt.close()

print "Done : {} : jets,evts {},{}".format(args.out,Size,Events)
	
