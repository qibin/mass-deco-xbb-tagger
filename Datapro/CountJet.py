import glob
import argparse
import os
import structDef as stdf
import modCount as ct


parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("--path", dest='path',  default="./test_data/", help="data path base")
parser.add_argument("--pattern", dest='pattern',  default="*.h5", help="data file name pattern")
parser.add_argument("--key", dest='key',  default="fat_jet", help="count of which jet")
args = parser.parse_args()


files = glob.glob(args.path+args.pattern)
flags = ("H","D","T")

key = args.key

if key == "fat_jet":
	print "Process fat_jet..."
elif key in stdf.subjet_type1_keys:
	print "Process subjet : type1 : {}".format(key)
elif key in stdf.subjet_type2_keys:
	print "Process subjet : type2 : {}".format(key)
else:
	print "This key {} is not convention. Caution.".format(key)

fileNum, jetNum, evtNum, wtNum = ct.Count(files,key)
for flag in flags:
	print "Total {} file:{}, jet:{}, evt:{}, wt:{}".format(flag, fileNum[flag], jetNum[flag], evtNum[flag], wtNum[flag])
