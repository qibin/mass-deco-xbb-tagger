import pandas as pd
import glob
import numpy as np
import argparse,math,os
import matplotlib.pyplot as plt
plt.switch_backend('agg')
import psutil
import gc
import cPickle as pk
from matplotlib.ticker import FormatStrFormatter

parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("--in", dest='inp',  default="./test/test_data/", help="path to in dir")
parser.add_argument("--out", dest='out',  default="./test/test_output/", help="pathout")
args = parser.parse_args()

files = glob.glob(args.inp + "/*_D.h5")
key="fat_jet"
variables=["pt","mcEventWeight"]
outfig=args.out+"JZXW_slice_logxy_wt_001.png"
outpdf=args.out+"JZXW_slice_logxy_wt_001.pdf"


def getJZX(filename):
#see https://gitlab.cern.ch/atlas-boosted-hbb/xbb-datasets/blob/master/p3652/mc16a-input-datasets.txt
#file name is like "user.dguest.16843503._000001.output_361020_D.h5"
	idString=filename.split("_")[-2]
	if len(idString)!=6 : 
		print "Error file : {}".format(filename)
		return None
	id=int(idString)
	X=id-361020 #361020=JZ0W, so on
	if X < 0 :
		print "Not JZXW Dijet : {}".format(filename)
		return None
	return X

Data={}
Wt={}
histLabel={}
Size=0
for file in files:
	if getJZX(file) is None : continue
	catlog="JZ{:0>2d}W".format(getJZX(file))
	data=pd.read_hdf(file,"fat_jet")[variables].dropna() #[[]] means return dataFrame
	size=data["pt"].size
	if catlog not in histLabel.keys() :
		histLabel[catlog]=catlog
		Data[catlog]=data["pt"] # [] return np.arrary
		Wt[catlog]=data["mcEventWeight"]
		print "processed {} : new jets {}".format(catlog,Data[catlog].size)
	else :
		Data[catlog]=np.concatenate((Data[catlog],data["pt"]))
		Wt[catlog]=np.concatenate((Wt[catlog],data["mcEventWeight"]))
		print "processed {} : append jets {}, total {}".format(catlog,size,Data[catlog].size)
	mems1=psutil.Process(os.getpid()).memory_info().rss
	Size+=size
	del data
	gc.collect()
	mems2=psutil.Process(os.getpid()).memory_info().rss
	print "mem:{}M -> {}M".format(mems1/1000000,mems2/1000000)
mems1=psutil.Process(os.getpid()).memory_info().rss
#DATA=np.zeros(Size)
#WT=np.zeros(Size)
Data["ToT"]=np.concatenate(Data.values())
Wt["ToT"]=np.concatenate(Wt.values())
histLabel["ToT"]="ToT"
mems2=psutil.Process(os.getpid()).memory_info().rss
print "MEM:{}M -> {}M".format(mems1/1000000,mems2/1000000)

outData=args.out+"Data.pickle"
outWt=args.out+"Wt.pickle"
fData= open(outData, 'wb')
fWt= open(outWt, 'wb')
pk.dump(Data,fData,pk.HIGHEST_PROTOCOL)  #using highest protocol: binary e.g.
pk.dump(Wt,fWt,pk.HIGHEST_PROTOCOL)
print "pickle finished: {}:{}M, {}:{}M".format(outData,os.path.getsize(outData)/1024/1024,outWt,os.path.getsize(outWt)/1024/1024)
fData.close()
fWt.close()

fig=plt.figure()
#fig.set_size_inches(20, 12)
ax=fig.gca()
#ax.hist(Data.values(),weights=histWeight.values(),bins=20,histtype="step",label=histLabel.values(),color=histColor.values())
ax.hist(Data.values(),weights=Wt.values(),bins=100,histtype="step",label=histLabel.values())
ax.legend(fancybox=True, framealpha=0.5)
ax.set_title("JZXW(p3652) Large-R jets (R=1.0, fat_jet)")
ax.legend(loc='upper right', fontsize="x-small")
#ax.set_yscale("log", nonposy="clip")
#ax.set_xscale("log", nonposx="clip")
ax.set_ylabel("Jets with mcEventWeight")
ax.set_xlabel("fat_jet pt")
#fig.show()
ax.xaxis.set_major_formatter(FormatStrFormatter('%3.2g'))
ax.yaxis.set_major_formatter(FormatStrFormatter('%3.2g'))
fig.savefig(outfig,dpi=300)
fig.savefig(outpdf,dpi=300)
print "Done : {} , jets {}".format(outfig,Size)
print "Done : {} , jets {}".format(outpdf,Size)	
	
