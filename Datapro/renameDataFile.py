import os
csvList="./HbbDijetsTop.csv"
dataDir="./data/"
destDir="./H_D_T_Datasets/"



with open(csvList, "rU") as f:
    lines = f.readlines()

sourceDatasetsWithFlags = [x.strip() for x in lines]


os.popen("mkdir -p " + destDir)
for sourceDatasetWithFlag in sourceDatasetsWithFlags:

    sourceDatasetAndFlag = sourceDatasetWithFlag.split(",")

    sourceDataset = sourceDatasetAndFlag[0]
    sourceDataset = sourceDataset.replace("cssksd.v2_output","ak10lc.v5_output")
    #use new data dump by Dan
    flag = sourceDatasetAndFlag[1]
    #H:Higgs Hbb; T:Top Ztt; D:Dijet production (QCD)

    dsid = sourceDatasetWithFlag.split(".")[2]

    print "processing " +  sourceDataset
    print "find single files..."
    output = os.popen("ls -1 " + dataDir + sourceDataset)

    #output = os.popen("rucio list-file-replicas " + sourceDataset + " --rse NERSC_LOCALGROUPDISK")

    for line in output.readlines():
        if "h5" in line:
            oldPath = dataDir + sourceDataset + "/" + line.strip('\n')
            oldFileName = oldPath.split("/")[-1]
            newFileName = oldFileName.split('.h5')[0] + "_" + dsid + "_" + flag + ".h5"
            newPath = destDir + newFileName
            #print "cp and rename: " + oldPath +" TO " + newPath
            #os.popen("cp " + oldPath + " " + newPath)
            absPath = os.getcwd() + "/" + oldPath.lstrip(".")
            print "just soft link file: " + absPath +" TO " + newPath
            os.popen("ln -s " + absPath + " " + newPath)


