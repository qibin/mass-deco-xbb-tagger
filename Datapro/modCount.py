#import pickle
import pandas as pd
#import matplotlib.pyplot as plt
#plt.switch_backend('agg')
import os
import psutil
import numpy as np
#from matplotlib.ticker import FormatStrFormatter
import gc
import h5py as h5

def Count(files,key):
	flags = ("H","T","D")
	flagDict = {"H":"Higgs","T":"Top","D":"Dijet"}
	fileNo = 0
	fileNum = {"H":0,"T":0,"D":0}
	jetNum = {"H":0,"T":0,"D":0}
	evtNum = {"H":0,"T":0,"D":0}
	wtNum  = {"H":0,"T":0,"D":0}
	for file in files:
		if file[-4] in flags:
			fileNo += 1
			flag=file[-4]
			fileNum[flag] += 1
                        print "processing {} : {} :: {}".format(fileNo,flag,file)
                else :
                        print "Cannot recognize file category: "+file+" Skip..."
                        continue
		jetNum[flag] += len(pd.read_hdf(file, key))
		metadata=h5.File(file,"r")["metadata"]
		evtNum[flag] += metadata["nEventsProcessed"]
		wtNum[flag]  += metadata["sumOfWeights"]
		del metadata
		gc.collect()
		print "Now {}  jet:{} evt:{} wt:{}".format(flag,jetNum[flag], evtNum[flag], wtNum[flag])
		print "MEM:{}".format(psutil.Process(os.getpid()).memory_info().rss)
	print "All files finished. File:{}::H{},D{},T{}".format(fileNo,fileNum["H"],fileNum["D"],fileNum["T"])
	return fileNum, jetNum, evtNum, wtNum
