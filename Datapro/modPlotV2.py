import pickle
import pandas as pd
import matplotlib.pyplot as plt
plt.switch_backend('agg')
import os
import numpy as np
from matplotlib.ticker import FormatStrFormatter
import gc

#princple: data is passed by, not by mem ot callback parameter.

def plotMultiHist(files,key,plotVariables, outpath, oname,*n): #for the mem safe, every variable is process on the disk
	print "obslete.....(run"
	return


def PlotMultiHist(files,key,plotVariables, varDict, outpath, oname,*n): #on the mem, assume each variable will not overflow the mem
	flags = ("H","T","D")
	flagDict = {"H":"Higgs","T":"Top","D":"Dijet"}
	colorDict ={"H":"black","T":"red","D":"lime"}
	print "Only use mem. Caution memory usage!!"
	temp = outpath+"_temp.h5" #will ont use this temp file, but check for safe
	if n is ():
		N=0
	 	outfig = outpath+"{}_{}.pdf".format(oname,key)
	else:
		N=n[0]
		outfig = outpath+"{}_{}_sup{}.pdf".format(oname,key,N)
	if len(plotVariables)<=0:
		return

	if len(plotVariables)>30:
		print "!!! slice {}:{}".format(key,N+1)
		PlotMultiHist(files,key,plotVariables[(N+1)*30:], varDict, outpath,oname,N+1)
	fig=plt.figure()
	fig.set_size_inches(20, 12)
	#fig.suptitle('Compare of all single variable of {}'.format(key))
	varNo=0
	for varName in plotVariables:
		eventCount={}
		Variable={}
		for flag in flags:
			Variable[flag]=pd.DataFrame(columns=[varName],dtype=varDict[varName])
		varNo+=1
		if varNo > 30:
			break
		fileNo=0
		print "Processing {} : {}".format(varNo,varName)
		if os.path.exists(temp):
			print "delete existing temp!"
			os.remove(temp)
		for file in files:
			fileNo+=1
			if file[-4] in flags: #!!!
				flag=file[-4]
				print "     processing {} : {} :: {}".format(fileNo,flag,file)
			else :
				print "     Cannot recognize file category: "+file+" Skip..."
				continue
			data=pd.read_hdf(file,key)
			variable=data[[varName]].dropna()
			Variable[flag]=pd.concat([Variable[flag],variable],axis='index')
			#print Variable[flag].info()
			del data
			del variable
			gc.collect()
		Data={}
		histWeight={}
		histLabel={}
		histColor={}
		ax=fig.add_subplot(5,6,varNo)
		#ax.set_ylim([0,1])
		for flag in flags:
			pData=Variable[flag][varName] #become np.arrary
			eventCount[flag]=pData.size
			if pData.size != 0:
				print "CountEvents : {} :: {}'s {} : {}".format(flag, key ,varName,pData.size)
				Data[flag]=pData #maybe use [] to keep the order??
				histWeight[flag]=np.zeros_like(pData) + 1. / pData.size
				#histLabel[flag]="{:<6}:{:.0e}".format(flagDict[flag],pData.size)
				histLabel[flag]=flagDict[flag]
				histColor[flag]=colorDict[flag]
				#print type(Data[flag])		
			else:
				print "Nothing here: {} :: {}'s {} !!".format(flag, key ,varName)
				continue
			del pData
			gc.collect()
		ax.hist(Data.values(),weights=histWeight.values(),bins=20,histtype="step",label=histLabel.values(),color=histColor.values()) #[]:keep order ;{}:keep logic
		#order of dict is not always keep garentee!!!!!!!!!!!!
		#ax.ticklabel_format(axis='both', style='', scilimits=None, useOffset=None, useLocale=None, useMathText=None)
		#ax.ticklabel_format(axis='both', style='sci')
		ax.legend(fancybox=True, framealpha=0.5)
		ax.tick_params(axis='x', which='major', labelsize=8)
		ax.tick_params(axis='y', which='major', labelsize=8)
		ax.set_title("{}_{}".format(key,varName),fontsize=10)
		ytext="Frac/"
		for flag in flags:
			ytext+="{}:{:.1e} ".format(flag,eventCount[flag])
		#ax.set_ylabel("Evt. Frac",fontsize=8)
		ax.set_ylabel(ytext,fontsize=8)
		ax.locator_params("y", nbins = 5)
		ax.locator_params("x", nbins = 5)
		ax.xaxis.set_major_formatter(FormatStrFormatter('%2.1g'))
		ax.yaxis.set_major_formatter(FormatStrFormatter('%2.1f'))
		del Data
		del Variable
		del histWeight
		gc.collect()
	print "varNo of {} is {}".format(key,varNo)
	fig.tight_layout(h_pad=0.8)
	#fig.show()
	fig.savefig(outfig,dpi=300)
	print "File saved : "+outfig
	return
