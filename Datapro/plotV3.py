import glob
import argparse
import os
import structDef as stdf
import modPlotV2 as plot


parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("--path", dest='path',  default="./test_data/", help="data path base")
parser.add_argument("--pattern", dest='pattern',  default="*.h5", help="data file name pattern")
parser.add_argument("--key", dest='key',  default="", help="name of jet")
parser.add_argument("--variable", dest='variable',  default="", help="specific single variable you want to draw")
parser.add_argument("--outpath", dest='outpath',  default="./test_output/", help="output path base")
parser.add_argument("--oname", dest='oname',  default="varHist", help="output name prefix")
parser.add_argument("--ondisk", dest='ondisk',  default="mem", help="use disk I/O and save as much memory(be slow)")
args = parser.parse_args()


files = glob.glob(args.path+args.pattern)

if args.key == "":
	keys=stdf.keys
else:
	keys=[args.key]

for key in keys:
	if key == "fat_jet":
		print "Process fat_jet..."
		varStruct=stdf.fat_jet_dtype
	elif key in stdf.subjet_type1_keys:
		print "Process subjet : type1 : {}".format(key)
		varStruct=stdf.subjet_type1_dtype
	elif key in stdf.subjet_type2_keys:
		print "Process subjet : type2 : {}".format(key)
		varStruct=stdf.subjet_type2_dtype
	else:
		print "This key {} cannot be draw by this py".format(key)
		continue
	varDict=dict(varStruct)

	if args.variable == "":
		plotVariables=varDict.keys()
		print "  plot {} all variable".format(args.key)
	else:
		plotVariables=[args.variable]
		print "  plot {}'s {} : {}".format(args.key, args.variable, varDict[args.variable])

	#plotVariables=()

	if(args.ondisk == "mem"):
		#run on mem, caution mem usage
		print "  Run on memory, caution memory overflow"
		plot.PlotMultiHist(files,key,plotVariables, varDict, args.outpath,args.oname)
	else:
		#run on disk, use disk cache so that mem will be save
		print "  Run on disk, caution disk I/O and R/W wear out"
		plot.plotMultiHist(files,key,plotVariables, args.outpath, args.oname)

